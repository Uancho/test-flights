import React from "react";
import { Link, Redirect } from "react-router-dom";
import Headroom from "headroom.js";
import {
  UncontrolledCollapse,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from "reactstrap";
import { Fragment } from "react";

class NavBar extends React.Component {
  constructor(){
    super();
    this.state = ({
      loggedUser: false,
      currentUser: null,
    });
  }
  componentDidMount() {
    let headroom = new Headroom(document.getElementById("navbar-main"));
    headroom.init();

    const loggedUser = localStorage.getItem('loggedUser');
    if(loggedUser){
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.setState({
        loggedUser: true,
        currentUser,
      });
    }
  }
  logout(){
    localStorage.removeItem('loggedUser');
    localStorage.removeItem('currentUser');
    window.location.assign('/');
  }
  render() {
    return (
      <>
        <header className="header-global">
          <Navbar
            className="navbar-main navbar-transparent navbar-light headroom"
            expand="lg"
            id="navbar-main"
          >
            <Container>
              <NavbarBrand className="mr-lg-5" to="/" tag={Link}>
                <img
                  alt="..."
                  src={require("assets/img/brand/ala-white.png")}
                />
              </NavbarBrand>
              <button className="navbar-toggler" id="navbar_global">
                <span className="navbar-toggler-icon" />
              </button>
              <UncontrolledCollapse navbar toggler="#navbar_global">
                <div className="navbar-collapse-header">
                  <Row>
                    <Col className="collapse-brand" xs="6">
                      <Link to="/">
                        <img
                          alt="..."
                          src={require("assets/img/brand/argon-react.png")}
                        />
                      </Link>
                    </Col>
                    <Col className="collapse-close" xs="6">
                      <button className="navbar-toggler" id="navbar_global">
                        <span />
                        <span />
                      </button>
                    </Col>
                  </Row>
                </div>


                {/* <Nav className="navbar-nav-hover align-items-lg-center" navbar>
                  <UncontrolledDropdown nav>
                    <DropdownToggle nav>
                      <i className="ni ni-collection d-lg-none mr-1" />
                      <span className="nav-link-inner--text">Examples</span>
                    </DropdownToggle>
                    <DropdownMenu>
                      <a href="https://demos.creative-tim.com/argon-design-system-react/?_ga=2.232874141.2089069479.1611252136-1954841027.1611252136#/">
                        Live template
                      </a>
                      <br/>
                      <a href="https://fontawesome.com/v4.7.0/icons/">
                        Fontawesome 4.7
                      </a>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav> */}


                <Nav className="align-items-lg-center ml-lg-auto" navbar>
                    {
                      this.state.loggedUser ? 
                      <Fragment>
                        <NavItem>
                          <NavLink to="/" tag={Link}
                            className="nav-link-icon"
                            onClick={e => e.preventDefault()}
                          >
                            {(this.state.currentUser 
                            && this.state.currentUser.name) || ''} 
                            , edad: {this.state.currentUser 
                            && getAge(this.state.currentUser.birthday)}
                          </NavLink>
                        </NavItem>
                          <NavItem>
                          <NavLink to="/logout" tag={Link}
                            className="nav-link-icon"
                            onClick={this.logout}
                          >
                            <i className="fa fa-sign-out" /> Salir
                          </NavLink>
                        </NavItem>
                      </Fragment> :
                      window.location.href.includes('/login-page') ?
                      <NavItem>
                        <NavLink to="/register-page" tag={Link}
                          className="nav-link-icon"
                        >
                          <i className="fa fa-user-plus" /> Registrarse
                        </NavLink>
                      </NavItem>
                      : <NavItem>
                        <NavLink to="/login-page" tag={Link}
                          className="nav-link-icon"
                        >
                          <i className="fa fa-sign-in" /> Ingresar
                        </NavLink>
                      </NavItem>
                    }
                </Nav>
              </UncontrolledCollapse>
            </Container>
          </Navbar>
        </header>
      </>
    );
  }
}
function getAge(dateString) {
  const today = new Date();
  const birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

export default NavBar;
