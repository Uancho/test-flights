import React from "react";
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import "./assets/vendor/nucleo/css/nucleo.css";
import "./assets/vendor/font-awesome/css/font-awesome.min.css";
import "./assets/scss/argon-design-system-react.scss";
import "./assets/css/styles.css";

import Index from './views/Index';
import Login from './views/IndexSections/Login';
import Register from './views/IndexSections/Register';

ReactDOM.render(
  <Router>
    <Switch>
      <Route path="/login-page">
        <Login />
      </Route>
      <Route path="/register-page">
        <Register />
      </Route>
      <Route path="/">
        <Index />
      </Route>
    </Switch>
  </Router>,
  document.getElementById("root")
);
