import React from "react";
import NavBar from "../components/Navbars/NavBar";
import Footer from "../components/Footers/SimpleFooter";

import Hero from "./IndexSections/Hero.jsx";

import FlightSearch from "./IndexSections/FlightSearch";

class Index extends React.Component {
  constructor(){
    super();
    this.state = ({
      loggedUser: false,
    });
  }
  storageUser(){
    const users = [
      {
        name: "Barry Allen",
        birthday: "Wed Jun 14 2000 00:00:00 GMT-0500",
        email: "allen@mail.com",
        password: "flash",
      },
      {
        name: "Bruce Wayne",
        birthday: "Mon May 22 1995 00:00:00 GMT-0500",
        email: "wayne@mail.com",
        password: "bat",
      },
      {
        name: "Tony Stark",
        birthday: "Sat Sep 19 1964 00:00:00 GMT-0500",
        email: "stark@mail.com",
        password: "iron",
      },
      {
        name: "Diana Prince",
        birthday: "Sun Dec 26 1940 00:00:00 GMT-0500",
        email: "prince@mail.com",
        password: "wonder",
      },
      {
        name: "Clark Kent",
        birthday: "Fri Aug 09 1991 00:00:00 GMT-0500",
        email: "kent@mail.com",
        password: "super",
      },
      {
        name: "Peter Parker",
        birthday: "Thu Nov 03 2005 00:00:00 GMT-0500",
        email: "parker@mail.com",
        password: "spider",
      },
    ];
    const jsonUsers = JSON.stringify(users);
    localStorage.setItem("users", jsonUsers);
  }
  componentDidMount() {
    const users = localStorage.getItem('users');
    if(!users){
      this.storageUser();
    }

    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;

   
    const loggedUser = localStorage.getItem('loggedUser');
    if(loggedUser){
      this.setState({
        loggedUser: true,
      });
    }


  }
  render() {
    return (
      <>
        <NavBar />
        <main ref="main">
          <Hero />
          { this.state.loggedUser ? <FlightSearch /> : null }
        </main>
        <Footer />
      </>
    );
  }
}

export default Index;
