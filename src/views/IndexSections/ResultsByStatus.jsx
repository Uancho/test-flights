import React, { useState, useEffect } from 'react';
import { Fragment } from 'react';
import { Table } from 'reactstrap';

export default function ResultsByStatus(props){
  const [tableBody, setTableBody] = useState(
    <tbody>
      <tr>
        <td className='text-center' colSpan={7}>
          Sin resultados
        </td>
      </tr>
    </tbody>
  );

  const dateFormatted = date => {
    const dateParsed = new Date(Date.parse(date));
    const year = dateParsed.getFullYear();
    const month = dateParsed.getMonth() + 1;
    const day = dateParsed.getDate();
    return year + '/' + (month < 10 ? '0' + month : month) + '/' + day;
  };

  const valueFormatted = value => {
    let noDecValue = new Intl.NumberFormat(
      "es-CO", 
      { style: "currency", currency: "COP" 
    }).format(Number(value));
    return noDecValue.toString().replace(',00','');
  }
  
  useEffect(() => {
    let idVuelo = (props && props.idVuelo -1) || '';

    fetch('/flights.json')
    .then(res => res.json())
    .then(data => {
      let dataFiltered = null;
      
      if (idVuelo){
        dataFiltered = data.filter(
          flight => flight.id === idVuelo
      )}

      if(!data || !data[idVuelo]) { return }

      dataFiltered = data[idVuelo];

      const dataMap = 
        <tbody>
          <tr>
            <th>{dataFiltered.id}</th>
            <td>{dataFiltered.origen}</td>
            <td>{dataFiltered.destino}</td>
            <td>{dateFormatted(dataFiltered.fecha)}</td>
            <td>{dataFiltered.hora}</td>
            <td>{valueFormatted(dataFiltered.valor)}</td>
            <td>{dataFiltered.aerolinea}</td>
          </tr>
          <tr>
            <td colSpan={7}>
              Estado: {dataFiltered.disponible ? 'DISPONIBLE' : 'NO DISPONIBLE'}
            </td>
          </tr>
        </tbody>;

      
      setTableBody(dataMap);
    })
    .catch(e => console.log('No se cargaron datos'));
  }, []);

  return(
    <Fragment>
      <h5>Estado de vuelo</h5>
      <Table size="sm" responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Origen</th>
            <th>Destino</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Valor</th>
            <th>Aerolínea</th>
            <th>$</th>
          </tr>
        </thead>
          {tableBody}
      </Table>
    </Fragment>
  )
};

  
function pay(flightData){
  const currentUser = JSON.parse(localStorage.getItem('currentUser')) || null;
  let payments = localStorage.payments || [];
  let paymentsParsed = [];
  if(payments && payments.length) {paymentsParsed = JSON.parse(payments);}

  const getAge = (dateString) => {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  const age = getAge((currentUser && currentUser.birthday) || new Date());

  if(age < 18) { 
    return alert('Los pagos no están permitidos para menores de edad');
  };

  const payment = {
    userEmail: currentUser.email, 
    flight: flightData.id, 
    flightTime: flightData.hora, 
  };
  
  const reservedFlight = paymentsParsed.filter(paym =>
    paym.userEmail === currentUser.email && paym.flightTime === flightData.hora
  );
  
  if(!reservedFlight || !reservedFlight.length){
    paymentsParsed.push(payment);
    localStorage.setItem('payments', JSON.stringify(paymentsParsed));
    alert('Vuelo pagado con éxito!');
  } else {
    alert('No puedes pagar el vuelo. Ya tienes uno a esa hora');
  };
};