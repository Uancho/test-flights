import React from "react";
import {
  FormGroup,
  Input,
  Row,
  Button,
  Col,
  Container,
} from "reactstrap";
import ResultsByTime from './ResultsByTime';
import ResultsByRate from './ResultsByRate';
import ResultsByStatus from "./ResultsByStatus";

class FlightSearch extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cities: null,
      origin: '',
      destination: '',
      idVuelo: 0,
      listItems: <></>,
      showResultsByTime: false,
      showResultsByRate: false,
      showResultsByStatus: false,
    };
  }
  state = {};

  componentDidMount(){
    const setcities = async () => {
      const res = await fetch('/colombian-cities.json');
      const cities = await res.json();
      this.setState({cities});
    };
    setcities();
  }

  handleReactDatetimeChange = (who, date) => {
    if (
      this.state.startDate &&
      who === "endDate" &&
      new Date(this.state.startDate._d + "") > new Date(date._d + "")
    ) {
      this.setState({
        startDate: date,
        endDate: date
      });
    } else if (
      this.state.endDate &&
      who === "startDate" &&
      new Date(this.state.endDate._d + "") < new Date(date._d + "")
    ) {
      this.setState({
        startDate: date,
        endDate: date
      });
    } else {
      this.setState({
        [who]: date
      });
    }
  };

  getClassNameReactDatetimeDays = date => {
    if (this.state.startDate && this.state.endDate) {
    }
    if (
      this.state.startDate &&
      this.state.endDate &&
      this.state.startDate._d + "" !== this.state.endDate._d + ""
    ) {
      if (
        new Date(this.state.endDate._d + "") > new Date(date._d + "") &&
        new Date(this.state.startDate._d + "") < new Date(date._d + "")
      ) {
        return " middle-date";
      }
      if (this.state.endDate._d + "" === date._d + "") {
        return " end-date";
      }
      if (this.state.startDate._d + "" === date._d + "") {
        return " start-date";
      }
    }
    return "";
  };

  handleInputCityChange(classname){
    const cityInput = document.querySelector(classname);
    const matchList = document.querySelector('.match-list');

    // const searchCities = searchText => {
    const searchCities = async searchText => {
      // const res = await fetch('/colombian-cities.json');
      // const cities = await res.json();
      const cities = this.state.cities;

      // let matches = cities.filter(city => {
      //   const regex = new RegExp(`^${searchText}`, 'gi');
      //   return city.name.match(regex);
      //   return city.name.match(regex) || city.iata.match(regex);
      // });

      let matches = cities.filter(city => {
        const cityNameNormalized = city.name.normalize('NFD')
          .replace(/[\u0300-\u036f]/g, "");
        const regex = new RegExp(`^${searchText}`, 'gi');
        return cityNameNormalized.match(regex) 
          || city.name.match(regex) || city.iata.match(regex);
      });

      if(!searchText.length){
        this.setState({listItems: <></>});
        matchList.style.display = 'none';
        cityInput.value = '';
        matches = [];
        matchList.innerHTML = '';
        return;
      };

      outputHtml(matches);
    };

    const outputHtml = matches => {
      if (matches && matches[0] && matches[0].name) {
        if(classname.includes('origin')){
          this.setState({origin: matches[0].name})
        } else {
          this.setState({destination: matches[0].name})
        }
      };


      if(matches.length){
        const listJsx = matches.map((match, i) => <div key={i} className="list-item" 
          onClick={() => this.setCity(classname, match.name)}
        >
          <span className="city-name">{match.name}</span> ({match.iata})
          <br/><small>{match.state}</small>
        </div>
        );

        this.setState({listItems: listJsx});
        matchList.style.display = 'block';
      };


    };
    
    cityInput.addEventListener('input', () => searchCities(cityInput.value));
  };

  setCity(classname, city){
    let cityInput = document.querySelector(classname);

    if (!cityInput.value) { return } 
    else {
      if(classname.includes('origin')){
        this.setState({origin: city});
      } else {
        this.setState({destination: city});
      }
      cityInput.value = city;
    };
    
    setTimeout(function(){ 
      const matchList = document.querySelector('.match-list');
      if(matchList){
        matchList.style.display = 'none';
      }
    }, 200);
  };

  showResultsByTime(){
    this.setState({showResultsByTime: false});
    setTimeout(() => {
      this.setState({
        showResultsByTime: true, 
        showResultsByRate: false,
        showResultsByStatus: false,
      })
    }, 200);
  }

  showResultsByRate(){
    this.setState({showResultsByRate: false});
    setTimeout(() => {
      this.setState({
        showResultsByTime: false, 
        showResultsByRate: true,
        showResultsByStatus: false,
      })
    }, 200);
  }

  showResultsByStatus(){
    this.setState({showResultsByStatus: false});
    const idVuelo = 
      document.querySelector('.id-vuelo').value || 0;
    this.setState({idVuelo});

    if(!idVuelo){
      alert('Debe ingresar un identificador de vuelo')
    } else {
      setTimeout(() => {
        this.setState({
          showResultsByTime: false, 
          showResultsByRate: false,
          showResultsByStatus: true,
        })
      }, 200);
    }
  }

  render() {
    return (
      <Container className="search-container">
        <h3 className="h4 text-success font-weight-bold mt-md mb-4">
          Búsqueda de vuelo
        </h3>

        <Row>
          <Col className="mt-4 mt-md-0" md="12">
            <Row>

              <Col sm={4} xs={12}>
                <FormGroup>
                  <Input 
                    className="origin" 
                    placeholder="Origen" 
                    type="text" 
                    onChange={() => this.handleInputCityChange('.origin')} 
                    onBlur={() => this.setCity('.origin', this.state.origin)}
                  />
                </FormGroup>
              </Col>

              <Col sm={4} xs={12}>
                <FormGroup>
                  <Input
                    className="destination" 
                    placeholder="Destino" 
                    type="text" 
                    onChange={() => this.handleInputCityChange('.destination')} 
                    onBlur={() => this.setCity('.destination', this.state.destination)}
                  />
                </FormGroup>
              </Col>

              <Col sm={4} xs={12}>
                <FormGroup>
                  <Input
                    min={1} 
                    max={1000}
                    className="id-vuelo" 
                    placeholder="ID Vuelo" 
                    type="number" 
                    // onChange={() => this.handleInputCityChange('.destination')} 
                    // onBlur={() => this.setCity('.destination', this.state.destination)}
                  />
                </FormGroup>
              </Col>

            </Row>
          </Col>
        </Row>
        
        <Row>
          <Col className="mt-4 mt-md-0" md="12">
            <Row>
              <Col sm={12} xs={12}>
                <div className="match-list">
                  {this.state.listItems}
                </div>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row>
          <Col sm={4} xs={12}>
            <Button
              className="btn-search"
              color="primary"
              type="button"
              onClick={() => this.showResultsByTime()}
            >
              <span className="btn-inner--icon mr-1">
                <i className="fa fa-clock-o" />
              </span>
              <span className="btn-inner--text">Horarios</span>
            </Button>
          </Col>
          <Col sm={4} xs={12}>
            <Button
              className="btn-search"
              color="primary"
              type="button"
              onClick={() => this.showResultsByRate()}
            >
              <span className="btn-inner--icon mr-1">
                <i className="fa fa-money" />
              </span>
              <span className="btn-inner--text">Tarifas</span>
            </Button>
          </Col>
          <Col sm={4} xs={12}>
            <Button
              className="btn-search"
              color="primary"
              type="button"
              onClick={() => this.showResultsByStatus()}
            >
              <span className="btn-inner--icon mr-1">
                <i className="fa fa-hourglass-start" />
              </span>
              <span className="btn-inner--text">Estado</span>
            </Button>
          </Col>
        </Row>
        {
          this.state.showResultsByTime ?
          <ResultsByTime 
            origin={this.state.origin} 
            destination={this.state.destination} 
          /> 
          : null
        }
        {
          this.state.showResultsByRate ?
          <ResultsByRate 
            origin={this.state.origin} 
            destination={this.state.destination} 
          /> 
          : null
        }
        {
          this.state.showResultsByStatus ?
          <ResultsByStatus idVuelo={this.state.idVuelo} /> 
          : null
        }
      </Container>
    );
  }
}

export default FlightSearch;
