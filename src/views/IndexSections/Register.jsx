import React from "react";
import {
  Alert,
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col
} from "reactstrap";
import ReactDatetime from "react-datetime";
import NavBar from "../../components/Navbars/NavBar";
import Footer from "../../components/Footers/SimpleFooter";

class Register extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      birthday: '',
      email: '',
      password: '',
      alertMsg: '',
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleBirthdayChange = this.handleBirthdayChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissAlert = this.dismissAlert.bind(this);
    this.getAge = this.getAge.bind(this);
  }
  handleNameChange(event) {
    this.setState({name: event.target.value});
  }
  // handleBirthdayChange(event) {
  //   this.setState({birthday: event.target.value});
  // }
  handleBirthdayChange(date) {
    // const age = this.getAge(date);
    // console.info('AGE:', age);
    this.setState({birthday: date});
  }
  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }
  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();

    const name = (this && this.state && this.state.name.trim()) || '';
    const birthday = (this && this.state && this.state.birthday) || '';
    const email = (this && this.state && this.state.email.trim()) || '';
    const password = (this && this.state && this.state.password) || '';

    if(!name || !email || !password) {
    this.setState({alertMsg: 'Completa correctamente todos los campos'});
      return;
    }


    /* DATA STORAGE */
        const getAllUsers = () => {
          return localStorage && localStorage.users;
        };
        let users = JSON.parse(getAllUsers());

        users.push({name, birthday, email, password});
        localStorage.setItem("users", JSON.stringify(users));
    /* DATA STORAGE */


    window.location.assign('/login-page');
  }
  dismissAlert(){
    this.setState({alertMsg: ''});
  }
  getAge(dateString) {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <NavBar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>

            {this.state.alertMsg ? <div 
                className="alert-div"
                onClick={this.dismissAlert}
              >
              <Alert color="warning">
                <span className="alert-inner--icon">
                  <i className="fa fa-exclamation-triangle" />
                </span>{" "}
                <span className="alert-inner--text">
                  {this.state.alertMsg}
                </span>
              </Alert>
            </div>
            : null}

            <Container className="pt-lg-md">
              <Row className="justify-content-center">
                <Col lg="5">
                  <Card className="bg-secondary shadow border-0">
                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small>Crea tu cuenta para acceder a nuestros servicios</small>
                      </div>
                      <Form onSubmit={this.handleSubmit} role="form">

                        <FormGroup>
                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-hat-3" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input 
                              placeholder="Nombre" 
                              type="text" 
                              value={this.state.name} 
                              onChange={this.handleNameChange}
                              onFocus={this.dismissAlert}
                              required 
                            />
                          </InputGroup>
                        </FormGroup>

                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-calendar-grid-58" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <ReactDatetime
                              dateFormat="DD-MM-YYYY"
                              onChange={this.handleBirthdayChange}
                              closeOnSelect={true}
                              inputProps={{
                                placeholder: "Fecha de nacimiento",
                                onFocus: this.dismissAlert,
                                required: true
                              }}
                              timeFormat={false}
                            />
                          </InputGroup>
                        </FormGroup>

                        {/* <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-calendar-grid-58" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="Fecha de nacimiento"
                              type="date"
                              value={this.state.birthday} 
                              onChange={this.handleBirthdayChange}
                              onFocus={this.dismissAlert}
                              required
                            />
                          </InputGroup>
                        </FormGroup> */}

                        <FormGroup>
                          <InputGroup className="input-group-alternative mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input 
                              placeholder="Correo electrónico" 
                              type="email" 
                              value={this.state.email} 
                              onChange={this.handleEmailChange}
                              onFocus={this.dismissAlert}
                              required />
                          </InputGroup>
                        </FormGroup>

                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="Contraseña"
                              type="password"
                              autoComplete="off" 
                              value={this.state.password} 
                              onChange={this.handlePasswordChange}
                              onFocus={this.dismissAlert}
                              required
                            />
                          </InputGroup>
                        </FormGroup>

                        <div className="text-center">
                          <Button
                            className="mt-4"
                            color="primary"
                          >
                            Crear cuenta
                          </Button>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </section>
        </main>
        <Footer />
      </>
    );
  }
}

export default Register;
