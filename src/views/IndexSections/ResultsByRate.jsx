import React, { useState, useEffect } from 'react';
import { Fragment } from 'react';
import { Table } from 'reactstrap';

export default function ResultsByRate(props){
  const [tableBody, setTableBody] = useState(
    <tr>
      <td className='text-center' colSpan={7}>
        Sin resultados
      </td>
    </tr>
  );

  const dateFormatted = date => {
    const dateParsed = new Date(Date.parse(date));
    const year = dateParsed.getFullYear();
    const month = dateParsed.getMonth() + 1;
    const day = dateParsed.getDate();
    return year + '/' + (month < 10 ? '0' + month : month) + '/' + day;
  };

  const valueFormatted = value => {
    let noDecValue = new Intl.NumberFormat(
      "es-CO", 
      { style: "currency", currency: "COP" 
    }).format(Number(value));
    return noDecValue.toString().replace(',00','');
  }
  
  useEffect(() => {
    let origin = (props && props.origin) || '';
    let destination = (props && props.destination) || '';

    fetch('/flights.json')
    .then(res => res.json())
    .then(data => {
      let dataFiltered = null;
      
      if (origin && destination){
        dataFiltered = data.filter(flight =>
        flight.origen === origin && flight.destino === destination
      )}
      if (origin && !destination){
        dataFiltered = data.filter(flight => flight.origen === origin
      )}
      if (!origin && destination){
        dataFiltered = data.filter(flight => flight.destino === destination
      )}

      if(!dataFiltered) { return };

      const compare = (a, b) => {
        if(a.valor < b.valor){
          return -1;
        }
        if(a.valor > b.valor){
          return 1;
        }
        return 0;
      };
      // const dataSort = dataFiltered;
      const dataSort = dataFiltered.sort(compare);

      const dataMap = dataSort.map((flight, i) => {
        return(<tr key={i}>
          <th>{flight.id}</th>
          <td>{flight.origen}</td>
          <td>{flight.destino}</td>
          <td>{dateFormatted(flight.fecha)}</td>
          <td>{flight.hora}</td>
          <td>{valueFormatted(flight.valor)}</td>
          <td>{flight.aerolinea}</td>
            {
              flight.disponible ? 
              <Fragment>
                <td className="pay-icon" onClick={() => pay(flight)}>
                  <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                </td>
                <td>
                  <i className="fa fa-check available" aria-hidden="true"></i>
                </td>
              </Fragment>
              : 
              <Fragment>
                <td className="pay-icon" onClick={() => alert('Vuelo sin disponibilidad')}>
                  <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                </td>
                <td>
                <i className="fa fa-times not-available" aria-hidden="true"></i>
                </td>
              </Fragment>
            }
        </tr>)
      });
      
      setTableBody(dataMap);
    })
    .catch(e => console.log('No se cargaron datos'));
  }, []);

  return(
    <Fragment>
      <h5>Resultados por tarifa</h5>
      <Table size="sm" responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Origen</th>
            <th>Destino</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Valor</th>
            <th>Aerolínea</th>
            <th>$</th>
            <th>🪑</th>
          </tr>
        </thead>
        <tbody>
          {tableBody}
        </tbody>
      </Table>
    </Fragment>
  )
};

  
function pay(flightData){
  const currentUser = JSON.parse(localStorage.getItem('currentUser')) || null;
  let payments = localStorage.payments || [];
  let paymentsParsed = [];
  if(payments && payments.length) {paymentsParsed = JSON.parse(payments);}

  const getAge = (dateString) => {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  const age = getAge((currentUser && currentUser.birthday) || new Date());

  if(age < 18) { 
    return alert('Los pagos no están permitidos para menores de edad');
  };

  const payment = {
    userEmail: currentUser.email, 
    flight: flightData.id, 
    flightTime: flightData.hora, 
  };
  
  const reservedFlight = paymentsParsed.filter(paym =>
    paym.userEmail === currentUser.email && paym.flightTime === flightData.hora
  );
  
  if(!reservedFlight || !reservedFlight.length){
    paymentsParsed.push(payment);
    localStorage.setItem('payments', JSON.stringify(paymentsParsed));
    alert('Vuelo pagado con éxito!');
  } else {
    alert('No puedes pagar el vuelo. Ya tienes uno a esa hora');
  };
};