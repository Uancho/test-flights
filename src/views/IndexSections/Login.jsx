import React from "react";
import { Link } from "react-router-dom";
import {
  Alert,
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
  NavLink,
} from "reactstrap";
import NavBar from "../../components/Navbars/NavBar";
import Footer from "../../components/Footers/SimpleFooter";

class Login extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      alertMsg: '',
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissAlert = this.dismissAlert.bind(this);
  }
  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }
  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();

    const email = (this && this.state && this.state.email.trim()) || '';
    const password = (this && this.state && this.state.password) || '';

    if(!email || !password) {
      this.setState({alertMsg: 'Completa correctamente todos los campos'})
      return;
    }



    /* GET DATA */
        const getAllUsers = () => {
          return localStorage && localStorage.users;
        };
        const users = JSON.parse(getAllUsers());

        const currentUser = users.filter(user =>
          user.email === email
        )[0] || null;
    /* GET DATA */



    if(!currentUser 
      || currentUser.email !== email 
      || currentUser.password !== password){
      this.setState({alertMsg: 'Verifica los datos de ingreso'})
    } else {
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      localStorage.setItem('loggedUser', true);
      window.location.assign('/');
    }
  }
  dismissAlert(){
    this.setState({alertMsg: ''});
  }
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <NavBar />
        <main ref="main">
          <section className="section section-shaped section-lg">
            <div className="shape shape-style-1 bg-gradient-default">
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>

          {this.state.alertMsg ? <div 
              className="alert-div"
              onClick={this.dismissAlert}
            >
            <Alert color="warning">
              <span className="alert-inner--icon">
                <i className="fa fa-exclamation-triangle" />
              </span>{" "}
              <span className="alert-inner--text">
                {this.state.alertMsg}
              </span>
            </Alert>
          </div>
          : null}

            <Container className="pt-lg-md">
              <Row className="justify-content-center">
                <Col lg="5">
                  <Card className="bg-secondary shadow border-0">
                    <CardBody className="px-lg-5 py-lg-5">
                      <div className="text-center text-muted mb-4">
                        <small>Ingresa tu correo electrónico y contraseña</small>
                      </div>
                      <Form onSubmit={this.handleSubmit} role="form">
                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input 
                              placeholder="Correo electrónico" 
                              type="email" 
                              value={this.state.email} 
                              onChange={this.handleEmailChange}
                              onFocus={this.dismissAlert}
                              required
                            />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="Contraseña"
                              type="password"
                              autoComplete="off"
                              value={this.state.password} 
                              onChange={this.handlePasswordChange}
                              onFocus={this.dismissAlert}
                              required
                            />
                          </InputGroup>
                        </FormGroup>
                        <div className="text-center">
                          <Button
                            className="my-4"
                            color="primary"
                          >
                            Entrar
                          </Button>
                        </div>
                      </Form>
                    </CardBody>
                  </Card>
                  <Row className="mt-3">
                    <Col xs="6">
                      <a
                        className="text-light"
                        href="/password-recovery"
                        onClick={e => e.preventDefault()}
                      >
                        <small>Olvidaste tu contraseña?</small>
                      </a>
                    </Col>
                    <Col className="text-right" xs="6">
                        <NavLink to="/register-page" tag={Link}
                          className="text-light"
                          style={{padding: '0'}}
                        >
                          <small>Crear una cuenta</small>
                        </NavLink>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </section>
        </main>
        <Footer />
      </>
    );
  }
}

export default Login;
