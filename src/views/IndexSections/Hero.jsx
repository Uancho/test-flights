import React from "react";
import { Button, Container, Row, Col } from "reactstrap";

class Hero extends React.Component {
  constructor(){
    super();
    this.state = ({
      loggedUser: false,
    });
  }
  componentDidMount(){
    const loggedUser = localStorage.getItem('loggedUser');
    if(loggedUser){
      this.setState({
        loggedUser: true,
      });
    }
  }
  render() {
    return (
      <>
        <div className="position-relative">
          <section className="section section-lg section-hero section-shaped">
            <div className="shape shape-style-1 shape-default">
              <span className="span-150" />
              <span className="span-50" />
              <span className="span-50" />
              <span className="span-75" />
              <span className="span-100" />
              <span className="span-75" />
              <span className="span-50" />
              <span className="span-100" />
              <span className="span-50" />
              <span className="span-100" />
            </div>
            <Container className="shape-container d-flex align-items-center py-lg">
              <div className="col px-0">
                <Row className="align-items-center justify-content-center">
                  <Col className="text-center" lg="6">
                    <img
                      alt="..."
                      className="img-fluid"
                      src={require("assets/img/brand/ala-white.png")}
                      style={{ width: "200px" }}
                    />
                    <p className="lead text-white mb-4">
                      Bienvenid@ al sistema de gestión de vuelos. 
                      Trabajamos continuamente para simplificarle las labores 
                      de: consultar vuelos, reservar vuelos y comprar su 
                      boleto en línea.
                    </p>
                  </Col>
                </Row>
                {
                  this.state.loggedUser ?
                  <></>
                  :
                  <Button
                    className="btn-icon btn-neutral btn-3 ml-1 top-btn"
                    color="default"
                    type="button"
                    onClick={() => window.location.assign('/register-page')}
                  >
                    <span className="btn-inner--icon mr-1">
                      <i className="fa fa-user-plus" />
                    </span>
                    <span className="btn-inner--text">Registrarse</span>
                  </Button>
                }


                {/* <FlightsGenerator /> */}


              </div>
            </Container>
            
            <div className="separator separator-bottom separator-skew zindex-100">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="fill-white"
                  points="2560 0 2560 100 0 100"
                />
              </svg>
            </div>
          </section>
        </div>
      </>
    );
  }
}

function FlightsGenerator(){
  const cities = [
    {
      "country": "Colombia",
      "iata": "AXM",
      "name": "Armenia / La Tebaida",
      "state": "Quindío"
    },
    {
      "country": "Colombia",
      "iata": "BAQ",
      "name": "Barranquilla / Soledad",
      "state": "Atlántico"
    },
    {
      "country": "Colombia",
      "iata": "BOG",
      "name": "Bogotá",
      "state": "Distrito Capital"
    },
    {
      "country": "Colombia",
      "iata": "BGA",
      "name": "Bucaramanga / Lebrija",
      "state": "Santander"
    },
    {
      "country": "Colombia",
      "iata": "CLO",
      "name": "Cali / Palmira",
      "state": "Valle del Cauca"
    },
    {
      "country": "Colombia",
      "iata": "CTG",
      "name": "Cartagena",
      "state": "Bolívar"
    },
    {
      "country": "Colombia",
      "iata": "CUC",
      "name": "Cúcuta",
      "state": "Norte de Santander"
    },
    {
      "country": "Colombia",
      "iata": "LET",
      "name": "Leticia",
      "state": "Amazonas"
    },
    {
      "country": "Colombia",
      "iata": "MDE",
      "name": "Medellín / Rionegro",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "PEI",
      "name": "Pereira",
      "state": "Risaralda"
    },
    {
      "country": "Colombia",
      "iata": "RCH",
      "name": "Riohacha",
      "state": "La Guajira"
    },
    {
      "country": "Colombia",
      "iata": "ADZ",
      "name": "San Andrés",
      "state": "San Andrés y Providencia"
    },
    {
      "country": "Colombia",
      "iata": "SMR",
      "name": "Santa Marta",
      "state": "Magdalena"
    },
    {
      "country": "Colombia",
      "iata": "APO",
      "name": "Apartadó / Carepa",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "AUC",
      "name": "Arauca",
      "state": "Arauca"
    },
    {
      "country": "Colombia",
      "iata": "EJA",
      "name": "Barrancabermeja",
      "state": "Santander"
    },
    {
      "country": "Colombia",
      "iata": "FLA",
      "name": "Florencia",
      "state": "Caquetá"
    },
    {
      "country": "Colombia",
      "iata": "IBE",
      "name": "Ibagué",
      "state": "Tolima"
    },
    {
      "country": "Colombia",
      "iata": "MZL",
      "name": "Manizales",
      "state": "Caldas"
    },
    {
      "country": "Colombia",
      "iata": "EOH",
      "name": "Medellín",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "MTR",
      "name": "Montería",
      "state": "Córdoba"
    },
    {
      "country": "Colombia",
      "iata": "NVA",
      "name": "Neiva",
      "state": "Huila"
    },
    {
      "country": "Colombia",
      "iata": "PSO",
      "name": "Pasto / Chachagüí",
      "state": "Nariño"
    },
    {
      "country": "Colombia",
      "iata": "PPN",
      "name": "Popayán",
      "state": "Cauca"
    },
    {
      "country": "Colombia",
      "iata": "PVA",
      "name": "Providencia",
      "state": "San Andrés y Providencia"
    },
    {
      "country": "Colombia",
      "iata": "PUU",
      "name": "Puerto Asís",
      "state": "Putumayo"
    },
    {
      "country": "Colombia",
      "iata": "UIB",
      "name": "Quibdó",
      "state": "Chocó"
    },
    {
      "country": "Colombia",
      "iata": "TCO",
      "name": "Tumaco",
      "state": "Nariño"
    },
    {
      "country": "Colombia",
      "iata": "VUP",
      "name": "Valledupar",
      "state": "Cesar"
    },
    {
      "country": "Colombia",
      "iata": "VVC",
      "name": "Villavicencio",
      "state": "Meta"
    },
    {
      "country": "Colombia",
      "iata": "EYP",
      "name": "Yopal",
      "state": "Casanare"
    },
    {
      "country": "Colombia",
      "iata": "ACD",
      "name": "Acandí",
      "state": "Chocó"
    },
    {
      "country": "Colombia",
      "iata": "ACR",
      "name": "Araracuara",
      "state": "Caquetá"
    },
    {
      "country": "Colombia",
      "iata": "BSC",
      "name": "Bahía Solano",
      "state": "Chocó"
    },
    {
      "country": "Colombia",
      "iata": "BUN",
      "name": "Buenaventura",
      "state": "Valle del Cauca"
    },
    {
      "country": "Colombia",
      "iata": "CAQ",
      "name": "Caucasia",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "CPB",
      "name": "Capurganá",
      "state": "Chocó"
    },
    {
      "country": "Colombia",
      "iata": "COG",
      "name": "Condoto",
      "state": "Chocó"
    },
    {
      "country": "Colombia",
      "iata": "PCE",
      "name": "Cumaribo",
      "state": "Vichada"
    },
    {
      "country": "Colombia",
      "iata": "EBG",
      "name": "El Bagre",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "GIR",
      "name": "Girardot / Flandes",
      "state": "Cundinamarca "
    },
    {
      "country": "Colombia",
      "iata": "GPI",
      "name": "Guapi",
      "state": "Cauca"
    },
    {
      "country": "Colombia",
      "iata": "IPI",
      "name": "Ipiales / Aldana",
      "state": "Nariño"
    },
    {
      "country": "Colombia",
      "iata": "LCR",
      "name": "La Chorrera",
      "state": "Amazonas"
    },
    {
      "country": "Colombia",
      "iata": "LMC",
      "name": "La Macarena",
      "state": "Meta"
    },
    {
      "country": "Colombia",
      "iata": "MCJ",
      "name": "Maicao",
      "state": "La Guajira"
    },
    {
      "country": "Colombia",
      "iata": "MVP",
      "name": "Mitú",
      "state": "Vaupés"
    },
    {
      "country": "Colombia",
      "iata": "NQU",
      "name": "Nuquí",
      "state": "Chocó"
    },
    {
      "country": "Colombia",
      "iata": "OCV",
      "name": "Ocaña",
      "state": "Norte de Santander"
    },
    {
      "country": "Colombia",
      "iata": "PTX",
      "name": "Pitalito",
      "state": "Huila"
    },
    {
      "country": "Colombia",
      "iata": "URA",
      "name": "Puerto Bolívar / Uribia",
      "state": "La Guajira"
    },
    {
      "country": "Colombia",
      "iata": "PDA",
      "name": "Puerto Inírida",
      "state": "Guainía"
    },
    {
      "country": "Colombia",
      "iata": "PCR",
      "name": "Puerto Carreño",
      "state": "Vichada"
    },
    {
      "country": "Colombia",
      "iata": "LQM",
      "name": "Puerto Leguízamo",
      "state": "Putumayo"
    },
    {
      "country": "Colombia",
      "iata": "OTU",
      "name": "Remedios",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "SJE",
      "name": "San José del Guaviare",
      "state": "Guaviare"
    },
    {
      "country": "Colombia",
      "iata": "SVI",
      "name": "San Vicente del Caguán",
      "state": "Caquetá"
    },
    {
      "country": "Colombia",
      "iata": "RVE",
      "name": "Saravena",
      "state": "Arauca"
    },
    {
      "country": "Colombia",
      "iata": "CZU",
      "name": "Sincelejo / Corozal",
      "state": "Sucre"
    },
    {
      "country": "Colombia",
      "iata": "SOX",
      "name": "Sogamoso",
      "state": "Boyacá"
    },
    {
      "country": "Colombia",
      "iata": "TME",
      "name": "Tame",
      "state": "Arauca"
    },
    {
      "country": "Colombia",
      "iata": "TLU",
      "name": "Tolú",
      "state": "Sucre"
    },
    {
      "country": "Colombia",
      "iata": "VGZ",
      "name": "Villa Garzón",
      "state": "Putumayo"
    },
    {
      "country": "Colombia",
      "iata": "ULQ",
      "name": "Tuluá",
      "state": "Valle del Cauca"
    },
    {
      "country": "Colombia",
      "iata": "GYM",
      "name": "Bogotá D. C. / Chía",
      "state": "Cundinamarca"
    },
    {
      "country": "Colombia",
      "iata": "CTO",
      "name": "Santander de quilichao",
      "state": "Cauca"
    },
    {
      "country": "Colombia",
      "iata": "CRC",
      "name": "Cartago",
      "state": "Valle del Cauca"
    },
    {
      "country": "Colombia",
      "iata": "IGO",
      "name": "Chigorodó",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "RAV",
      "name": "Cravo Norte",
      "state": "Arauca"
    },
    {
      "country": "Colombia",
      "iata": "MGN",
      "name": "Magangué",
      "state": "Bolívar"
    },
    {
      "country": "Colombia",
      "iata": "MCJ",
      "name": "Maicao",
      "state": "La Guajira"
    },
    {
      "country": "Colombia",
      "iata": "MQU",
      "name": "Mariquita",
      "state": "Tolima"
    },
    {
      "country": "Colombia",
      "iata": "MTB",
      "name": "Montelíbano",
      "state": "Córdoba"
    },
    {
      "country": "Colombia",
      "iata": "NCI",
      "name": "Necoclí",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "PYA",
      "name": "Paipa",
      "state": "Boyacá"
    },
    {
      "country": "Colombia",
      "iata": "PGT",
      "name": "Puerto Gaitán",
      "state": "Meta"
    },
    {
      "country": "Colombia",
      "iata": "PCC",
      "name": "Puerto Rico",
      "state": "Caquetá"
    },
    {
      "country": "Colombia",
      "iata": "SGL",
      "name": "San Gil",
      "state": "Santander"
    },
    {
      "country": "Colombia",
      "iata": "NPD",
      "name": "San Pedro de Urabá",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "TRB",
      "name": "Turbo",
      "state": "Antioquia"
    },
    {
      "country": "Colombia",
      "iata": "BOG",
      "name": "Bogotá",
      "state": "Distrito Capital"
    },
    {
      "country": "Colombia",
      "iata": "MQZ",
      "name": "Marandúa",
      "state": "Guaviare"
    },
    {
      "country": "Colombia",
      "iata": "PAL",
      "name": "Puerto Salgar",
      "state": "Cundinamarca"
    },
    {
      "country": "Colombia",
      "iata": "TQS",
      "name": "Solano",
      "state": "Caquetá"
    }
  ];
  const airlines = [
    'Avianca',
    'EasyFly',
    'Latam',
    'Satena',
    'Vivaair',
    'Wingo',
  ];
  const times = [
    '00:00','00:30',
    '01:00','01:30',
    '02:00','02:30',
    '03:00','03:30',
    '04:00','04:30',
    '05:00','05:30',
    '06:00','06:30',
    '07:00','07:30',
    '08:00','08:30',
    '09:00','09:30',
    '10:00','10:30',
    '11:00','11:30',
    '12:00','12:30',
    '13:00','13:30',
    '14:00','14:30',
    '15:00','15:30',
    '16:00','16:30',
    '17:00','17:30',
    '18:00','18:30',
    '19:00','19:30',
    '20:00','20:30',
    '21:00','21:30',
    '22:00','22:30',
    '23:00','23:30',
  ];

  let i = 1;
  let flights = [];
  while (i < 1001) {
    const randDate = new Date(new Date().getTime() + Math.random() * (new Date('2021-01-31').getTime() - new Date().getTime()));
    const indexAirline = Math.floor(Math.random() * airlines.length);
    const indexTime = Math.floor(Math.random() * times.length);
    const indexOriginCity = Math.floor(Math.random() * cities.length);
    const indexDestinationCity = Math.floor(Math.random() * cities.length);
    flights.push({
      id: i, 
      origen: cities[indexOriginCity].name, 
      origenIata: cities[indexOriginCity].iata, 
      destino: cities[indexDestinationCity].name, 
      destinoIata: cities[indexDestinationCity].iata,
      fecha: randDate,
      hora: times[indexTime],
      valor: (Math.floor(Math.random() * 40) + 5) * 10000,
      disponible: Math.random() < 0.5,
      aerolinea: airlines[indexAirline], 
    })
    i++;
  }

  return(
    <>
      <textarea rows="8" style={{width: '100%', fontSize: 'x-small'}}>
        {JSON.stringify(flights)}
      </textarea>
    </>
  )
}

export default Hero;
